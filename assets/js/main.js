$(document).ready(function(){
var lodging;
var cabins = [
        {"name":"Cabin 1","value":"cabin1"},
        {"name":"Cabin 2","value":"cabin2"},
        {"name":"Cabin 3","value":"cabin3"},
        {"name":"Cabin 4","value":"cabin4"},
        {"name":"Cabin 5","value":"cabin5"},
        {"name":"A Frame Cabin", "value":"aframe"}
    ];
var mobile  = [
        {"name":"3-2 Double Wide", "value":"doublewide"},
        {"name":"Mobile Home 2","value":"mobile2"},
        {"name":"Mobile Home 3","value":"mobile3"},
        {"name":"Mobile Home 4","value":"mobile4"},
        {"name":"Mobile Home 5","value":"mobile5"},
        {"name":"Mobile Home 6","value":"mobile6"},
        {"name":"Mobile Home 7","value":"mobile7"}
    ];
var motel  = [
        {"name":"Motel Room 5", "value":"motel5"},
        {"name":"Motel Room 6", "value":"motel6"},
        {"name":"Motel Room 7", "value":"motel7"}
    ];
var rvSlots  = [
        {"name":"RV-Slot 2", "value":"rv#2"},
        {"name":"RV-Slot 4", "value":"rv#4"},
        {"name":"RV-Slot 6", "value":"rv#6"},
        {"name":"RV-Slot 7", "value":"rv#7"},
        {"name":"RV-Slot 8", "value":"rv#8"},
        {"name":"RV-Slot 9", "value":"rv#9"},
        {"name":"RV-Slot 10", "value":"rv#10"},
        {"name":"RV-Slot 11", "value":"rv#11"},
        {"name":"RV-Slot 12", "value":"rv#12"},
        {"name":"RV-Slot 13", "value":"rv#13"},
        {"name":"RV-Slot 14", "value":"rv#14"},
        {"name":"RV-Slot 15", "value":"rv#15"},
        {"name":"RV-Slot 16", "value":"rv#16"},
        {"name":"RV-Slot 17", "value":"rv#17"},
        {"name":"RV-Slot 18", "value":"rv#18"},
        {"name":"RV-Slot 19", "value":"rv#19"},
        {"name":"RV-Slot 20", "value":"rv#20"},
        {"name":"RV-Slot 21", "value":"rv#21"},
        {"name":"RV-Slot 22", "value":"rv#22"},
        {"name":"RV-Slot 23", "value":"rv#23"},
        {"name":"RV-Slot 24", "value":"rv#24"},
        {"name":"RV-Slot 25", "value":"rv#25"},
        {"name":"RV-Slot 26", "value":"rv#26"},
        {"name":"RV-Slot 27", "value":"rv#27"},
        {"name":"RV-Slot 28", "value":"rv#28"},
        {"name":"RV-Slot 29", "value":"rv#29"},
        {"name":"RV-Slot 30", "value":"rv#30"},
        {"name":"RV-Slot 31", "value":"rv#31"},
        {"name":"RV-Slot 32", "value":"rv#32"},
        {"name":"RV-Slot 33", "value":"rv#33"},
        {"name":"RV-Slot 34", "value":"rv#34"},
        {"name":"RV-Slot 35", "value":"rv#35"},
        {"name":"RV-Slot 36", "value":"rv#36"},
        {"name":"RV-Slot 37", "value":"rv#37"},
        {"name":"RV-Slot 38", "value":"rv#38"},
        {"name":"RV-Slot 39", "value":"rv#39"},
        {"name":"RV-Slot 40", "value":"rv#40"},
        {"name":"RV-Slot 41", "value":"rv#41"},
        {"name":"RV-Slot 42", "value":"rv#42"},
        {"name":"RV-Slot 43", "value":"rv#43"},
        {"name":"RV-Slot 44", "value":"rv#44"},
        {"name":"RV-Slot 45", "value":"rv#45"},
        {"name":"RV-Slot 46", "value":"rv#46"},
        {"name":"RV-Slot 47", "value":"rv#47"},
        {"name":"RV-Slot 48", "value":"rv#48"},
        {"name":"RV-Slot 49", "value":"rv#49"},
        {"name":"RV-Slot 50", "value":"rv#50"},
        {"name":"RV-Slot 51", "value":"rv#51"},
        {"name":"RV-Slot 52", "value":"rv#52"},
        {"name":"RV-Slot 53", "value":"rv#53"}
    ];

// datepicker
    $( function() {
        $( "#datepicker" ).datepicker();
    });
// console.log(cabins);
// on change lodging choice
    $('#lodging').on('change',function(event){
        event.preventDefault();
        resetchoice1();
        resetchoice2();
        lodging = $(this).val();
        // console.log("lodging preferred changed to - "+lodging);

        loadchoice1(lodging);
    })
    $('#choice1').on('change',function(event){
        event.preventDefault();
        resetchoice2();
        var choice1 = $(this).val();
        // console.log('choice 1 is -'+ choice1);
        loadchoice2(lodging, choice1);
    })

// load choice 1 options
    function loadchoice1(lodging){
        if(lodging == 'cabin'){
            // console.log(cabins);
            for (var i in cabins) {
                // console.log(cabins[i].value);
                $('<option value="'+cabins[i].value+'">'+cabins[i].name+'</option>').appendTo('#choice1');

            }
        }
        if(lodging == 'mobile'){
            for (var j in mobile) {
                // console.log(mobile[j].value);
                $('<option value="'+mobile[j].value+'">'+mobile[j].name+'</option>').appendTo('#choice1');

            }
        }
        if(lodging == 'motel'){
            for (var j in motel) {
                // console.log(motel[j].value);
                $('<option value="'+motel[j].value+'">'+motel[j].name+'</option>').appendTo('#choice1');

            }
        }
        if(lodging == 'rv-slot'){
            for (var j in rvSlots) {
                // console.log(motel[j].value);
                $('<option value="'+rvSlots[j].value+'">'+rvSlots[j].name+'</option>').appendTo('#choice1');

            }
        }
        $('#choice1').removeAttr('disabled');

    }
    function loadchoice2(lodging, choice1){
        if(lodging == 'cabin'){
            // console.log(cabins);
            for (var i in cabins) {
                // console.log(cabins[i].value);
                if(choice1 == cabins[i].value){
                    // console.log('match');
                }else{
                    // console.log('no mat≥÷ch load rest of the choices');
                    $('<option value="'+cabins[i].value+'">'+cabins[i].name+'</option>').appendTo('#choice2');
                }
            }
        }
        if(lodging == 'mobile'){
            for (var j in mobile) {
                // console.log(mobile[j].value);
                if(choice1 == mobile[j].value){
                    // console.log('match');
                }else{
                    $('<option value="'+mobile[j].value+'">'+mobile[j].name+'</option>').appendTo('#choice2');
                }
            }
        }
        if(lodging == 'motel'){
            for (var j in motel) {
                // console.log(motel[j].value);
                if(choice1 == motel[j].value){
                    // console.log('match');
                }else{
                    $('<option value="'+motel[j].value+'">'+motel[j].name+'</option>').appendTo('#choice2');
                }
            }
        }
        if(lodging == 'rv-slot'){
            for (var j in rvSlots) {
                // console.log(motel[j].value);
                if(choice1 == rvSlots[j].value){
                    // console.log('match');
                }else{
                    $('<option value="'+rvSlots[j].value+'">'+rvSlots[j].name+'</option>').appendTo('#choice2');
                }
            }
        }
        $('#choice2').removeAttr('disabled');

    }

// reset Selections
    function resetchoice1(){
        $('#choice1').empty();
        $('<option value="null">-- Please Choose --</option>').appendTo('#choice1');
    }
    function resetchoice2(){
        $('#choice2').empty();
        $('<option value="null">-- Please Choose --</option>').appendTo('#choice2');
        $('#choice2').prop('disabled',"true");
    }

})
