<br>
<?php

?>
<div class="card w-100 mx-auto shadow p-3 mb-5 bg-white rounded">
  <div class="card-body">
      <div class="card-title text-center">
          <h2 class="mx-auto">Cabin Rentals at Possum Kingdom Lake</h2>
      </div>
      <div class="card-text ">
          <div class="row">
              <div class="col-sm-6">
                  <p class="ml-5">Our fully stocked cabins and mobile homes sleep anywhere from 4-20 people. You'll enjoy great views and unlimited rest and relaxation in our cabins that are within walking distance of the shoreline of Possum Kingdom Lake.</p>
                  <p></p>
                  <div class="text-center">
                    <a href="<?=root?>pricing" class="btn btn-lg  btn-primary mx-auto">Price List</a>
                  </div>

                  <!-- <p>The following Av</p> -->
              </div>
              <div class="col text-center">

                  <img src="<?=root?>assets/images/motel/map.png" width="65%" alt="" id="map" data-toggle="modal" data-target="#exampleModal">
                  <p> <sub>click for larger map</sub></p>

              </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-sm-6">
              <div class="card w-auto mx-auto mb-1">

                  <h5 class="card-header">Cabin #1</h5>
                  <?php
                    // imageslideshow('cabin1');
                  ?>
                  <div class="fotorama" data-nav="thumbs" data-allowfullscreen="true">
                     <?php imageslideshow('cabin1'); ?>
                    </div>
                    <div class="card-body">
                      <!-- <h5 class="card-title"></h5> -->
                      <p class="card-text">Two Rooms w/ one queen size bed & two full size beds, one sofa bed, kitchen, one bath, table & chairs.  Vehicle parking limitations <sup>1</sup>. (Maximum number allowed: 8)</p>
                      <ul class="list-group ">
                          <li class="list-group-item">Summer: $165/night</li>
                          <li class="list-group-item">Holiday: $195.50/night</li>
                          <li class="list-group-item">Fall: $121/night</li>
                      </ul>
                      <br>
                      <a href="<?=root?>reservations/?id=cabin1" class="btn btn-primary btn-lg btn-block">Book Cabin</a>
                    </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="card w-auto mx-auto mb-1">
                  <h5 class="card-header">Cabin #2</h5>
                  <div class="fotorama" data-nav="thumbs" data-allowfullscreen="true">
                     <?php imageslideshow('cabin2'); ?>
                    </div>
                  <div class="card-body">
                      <!-- <h5 class="card-title"></h5> -->
                      <p class="card-text">Two Rooms w/ one queen size bed & two full size beds, one sofa bed, kitchen, one bath, table & chairs, Limit 2 vehicles.  Vehicle parking limitations <sup>1</sup> (Maximum number allowed: 8)</p>
                      <ul class="list-group ">
                          <li class="list-group-item">Summer: $165/night</li>
                          <li class="list-group-item">Holiday: $195.50/night</li>
                          <li class="list-group-item">Fall: $121/night</li>
                      </ul>
                      <br>
                      <a href="<?=root?>reservations/?id=cabin2" class="btn btn-primary btn-lg btn-block">Book Cabin</a>
                    </div>
              </div>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-sm-6">
              <div class="card w-auto mx-auto mb-1">
                  <h5 class="card-header">Cabin #3</h5>
                  <div class="fotorama" data-nav="thumbs" data-allowfullscreen="true">
                     <?php imageslideshow('cabin3'); ?>
                    </div>
                    <div class="card-body">
                      <!-- <h5 class="card-title"></h5> -->
                      <p class="card-text">Two rooms w/ one queen size bed & three full size beds, kitchen, one bath, table & chairs. Vehicle parking limitations <sup>1</sup> (Maximum number allowed: 8)</p>
                      <ul class="list-group ">
                          <li class="list-group-item">Summer: $165/night</li>
                          <li class="list-group-item">Holiday: $195.50/night</li>
                          <li class="list-group-item">Fall: $121/night</li>
                      </ul>
                      <br>
                      <a href="<?=root?>reservations/?id=cabin3" class="btn btn-primary btn-lg btn-block">Book Cabin</a>
                    </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="card w-auto mx-auto mb-1">
                  <h5 class="card-header">Cabin #4</h5>
                  <div class="fotorama" data-nav="thumbs" data-allowfullscreen="true">
                     <?php imageslideshow('cabin4'); ?>
                    </div>
                    <div class="card-body">
                      <!-- <h5 class="card-title"></h5> -->
                      <p class="card-text">One room with four full size beds, one bath, table and chairs. Vehicle parking limitations <sup>1</sup> (Maximum number allowed: 8)</p>
                      <ul class="list-group ">
                          <li class="list-group-item">Summer: $165/night</li>
                          <li class="list-group-item">Holiday: $195.50/night</li>
                          <li class="list-group-item">Fall: $121/night</li>
                      </ul>
                      <br>
                      <a href="<?=root?>reservations/?id=cabin4" class="btn btn-primary btn-lg btn-block">Book Cabin</a>
                    </div>
              </div>
            </div>
            <div class="col-sm-12 text-center">
                <h5>Cabins #3 & #4 may be rented together.(Sleeps 16 Combined)</h5>
                <p>Summer Rate: $319.00<br>
                Holiday Rate: $385.25<br>
                Fall Rate: $220.00</p>


            </div>
          </div>
          <hr>
          <div class="row">

            <div class="col-sm-6">
              <div class="card w-auto mx-auto mb-1">
                  <h5 class="card-header">Cabin #5</h5>
                  <div class="fotorama" data-nav="thumbs" data-allowfullscreen="true">
                     <?php imageslideshow('cabin5'); ?>
                    </div>
                    <div class="card-body">
                      <!-- <h5 class="card-title"></h5> -->
                      <p class="card-text">Three rooms w/ three queen size beds, one full size bed, three bunk beds, two futons in living room, two full size baths, full kitchen, table & chairs. Vehicle parking limitations <sup>1</sup> (Maximum Number Allowed: 20)</p>
                      <ul class="list-group ">
                          <li class="list-group-item">Summer: $319/night</li>
                          <li class="list-group-item">Holiday: $385.25/night</li>
                          <li class="list-group-item">Fall: $220/night</li>
                      </ul>
                      <br>
                      <a href="<?=root?>reservations/?id=cabin5" class="btn btn-primary btn-lg btn-block">Book Cabin</a>
                    </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="card w-auto mx-auto mb-1">
                  <h5 class="card-header">A-Frame House</h5>
                  <div class="fotorama" data-nav="thumbs" data-allowfullscreen="true">
                     <?php imageslideshow('aframe'); ?>
                    </div>
                    <div class="card-body">
                      <!-- <h5 class="card-title"></h5> -->
                      <p class="card-text">Two stories-upstairs there is one full size bed, two twin beds, & one bath. Downstairs-has one bedroom w/ two full size beds, one bath, full kitchen w/ bar that separates living area which has one sofa bed, couch, table & chairs. Also has a private back patio overlooking the lake. Vehicle parking limitations <sup>1</sup> (Maximum Number Allowed: 10)</p>
                      <ul class="list-group ">
                          <li class="list-group-item">Summer: $264/night</li>
                          <li class="list-group-item">Holiday: $316.25/night</li>
                          <li class="list-group-item">Fall: $198/night</li>
                      </ul>
                      <br>
                      <a href="<?=root?>reservations/?id=aframe" class="btn btn-primary btn-lg btn-block">Book Cabin</a>
                    </div>
              </div>
            </div>
          </div>
<hr>
          <div class="row">
            <div class="col-sm-6">
              <div class="card w-auto mx-auto mb-1">
                  <h5 class="card-header">Double Wide</h5>
                  <div class="fotorama" data-nav="thumbs" data-allowfullscreen="true">
                     <?php imageslideshow('doublewide'); ?>
                    </div>
                    <div class="card-body">
                      <!-- <h5 class="card-title"></h5> -->
                      <p>Six twin beds, two full beds, two queen beds, one fold out futon, & one hideaway bed. Waterfront deck overlooking the lake w/ charcoal grill. Vehicle parking limitations <sup>1</sup> (Sleeps 18)</p>
                      <ul class="list-group ">
                          <li class="list-group-item">Summer: $319/night</li>
                          <li class="list-group-item">Holiday: $385.25/night</li>
                          <li class="list-group-item">Fall: $220/night</li>
                      </ul>
                      <br>
                      <a href="<?=root?>reservations/?id=doublewide" class="btn btn-primary btn-lg btn-block">Book Cabin</a>
                    </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="card w-auto mx-auto mb-1">
                  <h5 class="card-header">Mobile Home #2</h5>
                  <div class="fotorama" data-nav="thumbs" data-allowfullscreen="true">
                     <?php imageslideshow('mobile2'); ?>
                    </div>
                    <div class="card-body">
                      <!-- <h5 class="card-title"></h5> -->
                      <p>One bedroom w/ one queen size bed, living area w/ a sofa bed, kitchen, one bath, table & chairs. Limit 1 vehicle at cabin. Vehicle parking limitations <sup>1</sup>. (Maximum Number Allowed: 4)</p>
                      <ul class="list-group ">
                          <li class="list-group-item">Summer: $115.50/night</li>
                          <li class="list-group-item">Holiday: $143.75/night</li>
                          <li class="list-group-item">Fall: $88/night</li>
                      </ul>
                      <br>
                      <a href="<?=root?>reservations/?id=mobile2" class="btn btn-primary btn-lg btn-block">Book Cabin</a>
                    </div>
              </div>
            </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-sm-6">
          <div class="card w-auto mx-auto mb-1">
              <h5 class="card-header">Mobile Home #3</h5>
              <div class="fotorama" data-nav="thumbs" data-allowfullscreen="true">
                 <?php imageslideshow('mobile3'); ?>
                </div>
                <div class="card-body">
                  <!-- <h5 class="card-title"></h5> -->
                  <p class="card-text">Three bedroom, one bath, fully equipped kitchen, three full size beds, one sofa bed. #3 has two baths & is located on a secluded cove. Vehicle parking limitations <sup>1</sup> (Maximum Number Allowed: 8)</p>
                  <ul class="list-group ">
                      <li class="list-group-item">Summer: $165/night</li>
                      <li class="list-group-item">Holiday: $195.50/night</li>
                      <li class="list-group-item">Fall: $121/night</li>
                  </ul>
                  <br>
                  <a href="<?=root?>reservations/?id=mobile3" class="btn btn-primary btn-lg btn-block">Book Cabin</a>
                </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="card w-auto mx-auto mb-1">
              <h5 class="card-header">Mobile Home #4</h5>
              <div class="fotorama" data-nav="thumbs" data-allowfullscreen="true">
                 <?php imageslideshow('mobile4'); ?>
                </div>
                <div class="card-body">
                  <!-- <h5 class="card-title"></h5> -->
                  <p class="card-text">Three bedroom, one bath, fully equipped kitchen, three full size beds, one sofa bed. #3 has two baths & is located on a secluded cove. Vehicle parking limitations <sup>1</sup> (Maximum Number Allowed: 8)</p>
                  <ul class="list-group ">
                      <li class="list-group-item">Summer: $165/night</li>
                      <li class="list-group-item">Holiday: $195.50/night</li>
                      <li class="list-group-item">Fall: $121/night</li>
                  </ul>
                  <br>
                  <a href="<?=root?>reservations/?id=mobile4" class="btn btn-primary btn-lg btn-block">Book Cabin</a>
                </div>
          </div>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-sm-6">
          <div class="card w-auto mx-auto mb-1">
              <h5 class="card-header">Mobile Home #5</h5>
              <div class="fotorama" data-nav="thumbs" data-allowfullscreen="true">
                 <?php imageslideshow('mobile4'); ?>
                </div>
                <div class="card-body">
                  <!-- <h5 class="card-title"></h5> -->
                  <p class="card-text">Three bedroom, one bath, fully equipped kitchen, three full size beds, one sofa bed. #3 has two baths & is located on a secluded cove. Vehicle parking limitations <sup>1</sup> (Maximum Number Allowed: 8)</p>
                  <ul class="list-group ">
                      <li class="list-group-item">Summer: $165/night</li>
                      <li class="list-group-item">Holiday: $195.50/night</li>
                      <li class="list-group-item">Fall: $121/night</li>
                  </ul>
                  <br>
                  <a href="<?=root?>reservations/?id=mobile5" class="btn btn-primary btn-lg btn-block">Book Cabin</a>
                </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="card w-auto mx-auto mb-1">
              <h5 class="card-header">Mobile Home #6</h5>
              <div class="fotorama" data-nav="thumbs" data-allowfullscreen="true">
                 <?php imageslideshow('mobile6'); ?>
                </div>
                <div class="card-body">
                  <!-- <h5 class="card-title"></h5> -->
                  <p class="card-text">Three bedroom, one bath, fully equipped kitchen, three full size beds, one sofa bed. #3 has two baths & is located on a secluded cove. Vehicle parking limitations <sup>1</sup> (Maximum Number Allowed: 8)</p>
                  <ul class="list-group ">
                      <li class="list-group-item">Summer: $165/night</li>
                      <li class="list-group-item">Holiday: $195.50/night</li>
                      <li class="list-group-item">Fall: $121/night</li>
                  </ul>
                  <br>
                  <a href="<?=root?>reservations/?id=mobile6" class="btn btn-primary btn-lg btn-block">Book Cabin</a>
                </div>
          </div>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col">
          <div class="card w-100 mx-auto mb-1">
              <h5 class="card-header">Mobile Home #7</h5>

                <div class="card-body">
                  <!-- <h5 class="card-title"></h5> -->
                  <div class="fotorama float-left mx-2" data-nav="thumbs" data-allowfullscreen="true">
                     <?php imageslideshow('mobile7'); ?>
                    </div>
                  <p class="card-text">Three bedroom, one bath, fully equipped kitchen, three full size beds, one sofa bed. #3 has two baths & is located on a secluded cove. Vehicle parking limitations <sup>1</sup> (Maximum Number Allowed: 8)</p>
                  <ul class="list-group ">
                      <li class="list-group-item">Summer: $165/night</li>
                      <li class="list-group-item">Holiday: $195.50/night</li>
                      <li class="list-group-item">Fall: $121/night</li>
                  </ul>
                  <br>
                  <a href="<?=root?>reservations/?id=mobile7" class="btn btn-primary btn-lg btn-block">Book Cabin</a>
                </div>
          </div>
        </div>

      </div>
  </div>
  <div class="">
      <sub>* Reservations are made for two nights on regular weekends and three nights on holiday weekends. One night deposit is required for reservations. Cancellations must be made 3 weeks in advance to receive a refund less a $50.00 Cancellation fee. Additional people over maximum number allowed will be charged $10.00 per person per day. Please see Policies Page for further fees and regulations.</sub>
      <br><sub><sup>1</sup> Parking area around certain cabins is limited. If more then the maximum number of vehicles, they are to be parked in the designated parking area. Thank you.</sub>
  </div>
</div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
      <div class="modal-body">
        <img src="<?=root?>assets/images/map.jpg"  alt="" id="map">
      </div>

    </div>
  </div>
</div>
