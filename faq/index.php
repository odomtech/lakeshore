<?php
define('root','../');
$URL = "FAQ's";
include(root.'includes/config.php');
include(root.'templates/header.php');
// Logo - Social/Connect Bar
include(root.'templates/socialbar.php');
// Navbar
include(root.'templates/navbar.php');
?>
<br>
<div class="container">
    <div class="card w-100 mx-auto shadow p-3 mb-5 bg-white rounded">
        <div class="card-body">
            <div class="card-title">
                <h2 class="text-center">FREQUENTLY ASKED QUESTIONS</h2>
                <div class="accordion" id="accordionExample">
                    <div class="card">
                    <div class="card-header" id="headingOne">
                      <h5 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                         <strong>Where are you located?</strong>
                        </button>
                      </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                      <div class="card-body">
                        We are approximately 110 miles Northwest of Ft. Worth, 90 miles South of Wichita Falls, 100 miles Northeast of Abilene at 5501 Lakeshore Drive, 24 miles Southeast of Graham, Texas on Possum Kingdom Lake. <strong><u><a href="<?=root?>directions">Click here for Directions!</a></u></strong>
                      </div>
                    </div>
                    </div>
                    <div class="card">
                    <div class="card-header" id="headingTwo">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                          <strong>What are the Office Hours?</strong>
                        </button>
                      </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                      <div class="card-body">
                          <br>Monday - CLOSED (except for Emergencies or problems)
                          <br>Tuesday - CLOSED (call for availability)
                          <br>Wednesday - CLOSED (call for availability)
                          <br>Thursday - 9am to 4pm
                          <br>Friday - 9am to 5pm
                          <br>Saturday - 9am to 5pm
                          <br>Sunday - 9am to 12 noon (9am to 5pm holiday weekends)
                          <br>Closed for Lunch from 12 noon to 1pm
                      </div>
                    </div>
                    </div>
                    <div class="card">
                    <div class="card-header" id="headingTwo">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse21" aria-expanded="false" aria-controls="collapseTwo">
                         <strong>When is check-in and check-out?</strong>
                        </button>
                      </h5>
                    </div>
                    <div id="collapse21" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                      <div class="card-body">
                          Check-in is 2:00 p.m.; check-out is 12:00 p.m. If you need to arrive later than 5:00 p.m., please let us know so that we can plan accordingly.
                      </div>
                    </div>
                    </div>
                    <div class="card">
                    <div class="card-header" id="headingTwo">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapseTwo">
                          <strong>What is there to do there?</strong><br>
                        </button>
                      </h5>
                    </div>
                    <div id="collapse3" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                      <div class="card-body">
                          We offer swimming and fishing. In the surrounding area you can rent canoes and boats. There are three (3) golf courses in the area, as well as antique shopping in several nearby towns. The nearest bowling alley and movie theater are located in Graham about 24 miles away.
                      </div>
                    </div>
                    </div>
                    <div class="card">
                    <div class="card-header" id="headingTwo">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapseTwo">
                          <strong>Are your cabins on the water?</strong>
                        </button>
                      </h5>
                    </div>
                    <div id="collapse4" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                      <div class="card-body">
                          Our cabins are within walking distance of the lake.
                      </div>
                    </div>
                    </div>
                    <div class="card">
                    <div class="card-header" id="headingTwo">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapseTwo">
                          <strong>Do you have Internet service available?</strong>
                        </button>
                      </h5>
                    </div>
                    <div id="collapse5" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                      <div class="card-body">
                          Yes, we now have Wi-fi service available.
                      </div>
                    </div>
                    </div>
                    <div class="card">
                    <div class="card-header" id="headingTwo">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapseTwo">
                          <strong>Do all the cabins have television?</strong>
                        </button>
                      </h5>
                    </div>
                    <div id="collapse6" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                      <div class="card-body">
                          No we do not have TV's.
                      </div>
                    </div>
                    </div>
                    <div class="card">
                    <div class="card-header" id="headingTwo">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse7" aria-expanded="false" aria-controls="collapseTwo">
                          <strong>What linens do you supply?</strong>
                        </button>
                      </h5>
                    </div>
                    <div id="collapse7" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                      <div class="card-body">
                          We provide bed linens (sheets, blankets, pillows).
                      </div>
                    </div>
                    </div>
                    <div class="card">
                    <div class="card-header" id="headingTwo">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse8" aria-expanded="false" aria-controls="collapseTwo">
                          <strong>Do you supply dishes and utensils?</strong>
                        </button>
                      </h5>
                    </div>
                    <div id="collapse8" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                      <div class="card-body">
                          We provide basic dishes and a single cooking pot.
                      </div>
                    </div>
                    </div>
                    <div class="card">
                    <div class="card-header" id="headingTwo">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse9" aria-expanded="false" aria-controls="collapseTwo">
                          <strong style="white-space: normal;">Do the cabins have coffee pots?</strong>
                        </button>
                      </h5>
                    </div>
                    <div id="collapse9" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                      <div class="card-body">
                          All cabins have coffee makers.
                      </div>
                    </div>
                    </div>
                    <div class="card">
                    <div class="card-header" id="headingTwo">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse10" aria-expanded="false" aria-controls="collapseTwo">
                         <strong style="white-space: normal;">Do you accept debit cards?</strong>
                        </button>
                      </h5>
                    </div>
                    <div id="collapse10" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                      <div class="card-body">
                          We do accept debit cards as payment.
                      </div>
                    </div>
                    </div>
                    <div class="card">
                    <div class="card-header" id="headingTwo">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse11" aria-expanded="false" aria-controls="collapseTwo">
                         <strong style="white-space: normal;">Will you charge my card when I make a reservations?</strong>
                        </button>
                      </h5>
                    </div>
                    <div id="collapse11" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                      <div class="card-body">
                          We run an authorization of 50% of the amount of your full saty, at the time of reservation.
                      </div>
                    </div>
                    </div>
                    <div class="card">
                    <div class="card-header" id="headingTwo">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse12" aria-expanded="false" aria-controls="collapseTwo">
                         <strong>Do you have a boat dock?</strong>
                        </button>
                      </h5>
                    </div>
                    <div id="collapse12" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                      <div class="card-body">
                          We have a 10 slip dock. Reservations are required.
                      </div>
                    </div>
                    </div>
                    <div class="card">
                    <div class="card-header" id="headingTwo">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse13" aria-expanded="false" aria-controls="collapseTwo">
                         <strong>Are campfires allowed?</strong>
                        </button>
                      </h5>
                    </div>
                    <div id="collapse13" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                      <div class="card-body">
                          No ground fires or fire pits are allowed on resort property.
                      </div>
                    </div>
                    </div>
                    <div class="card">
                    <div class="card-header" id="headingTwo">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse14" aria-expanded="false" aria-controls="collapseTwo">
                         <strong>Are fireworks allowed?</strong>
                        </button>
                      </h5>
                    </div>
                    <div id="collapse14" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                      <div class="card-body">
                          No fireworks are allowed on resort property.
                      </div>
                    </div>
                    </div>
                    <div class="card">
                    <div class="card-header" id="headingTwo">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse15" aria-expanded="false" aria-controls="collapseTwo">
                         <strong>Are ATV's allowed?</strong>
                        </button>
                      </h5>
                    </div>
                    <div id="collapse15" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                      <div class="card-body">
                          No ATV's or UTV's are allowed on resort property.
                      </div>
                    </div>
                    </div>
                    <div class="card">
                    <div class="card-header" id="headingTwo">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse16" aria-expanded="false" aria-controls="collapseTwo">
                         <strong>Is tent camping allowed?</strong>
                        </button>
                      </h5>
                    </div>
                    <div id="collapse16" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                      <div class="card-body">
                          No tent camping is allowed on resort property.
                      </div>
                    </div>
                    </div>
                    <div class="card">
                    <div class="card-header" id="headingTwo">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse17" aria-expanded="false" aria-controls="collapseTwo">
                         <strong>is there a grocery store nearby?</strong>
                        </button>
                      </h5>
                    </div>
                    <div id="collapse17" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                      <div class="card-body">
                          A convenience store is within ten (10) miles and Graham is twenty-four (24) miles away. There is also a small store on site with ice, soda, snacks, and a small selection of general merchandise.
                      </div>
                    </div>
                    </div>
                    <div class="card">
                    <div class="card-header" id="headingTwo">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse18" aria-expanded="false" aria-controls="collapseTwo">
                         <strong>Do you sell bait and fishing license?</strong>
                        </button>
                      </h5>
                    </div>
                    <div id="collapse18" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                      <div class="card-body">
                          Yes - frozen bait, no live bait. No - there is a 1800 phone number available to call for licenses.
                      </div>
                    </div>
                    </div>
                    <div class="card">
                    <div class="card-header" id="headingTwo">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse19" aria-expanded="false" aria-controls="collapseTwo">
                         <strong>Can I bring my pet?</strong>
                        </button>
                      </h5>
                    </div>
                    <div id="collapse19" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                      <div class="card-body">
                          All pets MUST be on a leash at all times. There is a $10.00 per day charge for pets. Pet owner is responsible for damages and cleaning up after pet.
                      </div>
                    </div>
                    </div>
                    <div class="card">
                    <div class="card-header" id="headingTwo">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse20" aria-expanded="false" aria-controls="collapseTwo">
                         <strong>Are there restaurant nearby?</strong>
                        </button>
                      </h5>
                    </div>
                    <div id="collapse20" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                      <div class="card-body">
                          Several restaurants are within ten (10) miles. Graham, with a wide choice of restaurants, is twenty-four (24) miles away.
                      </div>
                    </div>
                    </div>
                    <div class="card">
                    <div class="card-header" id="headingTwo">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse22" aria-expanded="false" aria-controls="collapseTwo">
                         <strong>Will my cell phone work?</strong>
                        </button>
                      </h5>
                    </div>
                    <div id="collapse22" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                      <div class="card-body">
                          Some providers have service, but a few may not have digital service on the resort property. Call your provider for more information.
                      </div>
                    </div>
                    </div>
                    <div class="card">
                    <div class="card-header" id="headingTwo">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse23" aria-expanded="false" aria-controls="collapseTwo">
                         <strong style="white-space: normal;">Are there phones in the rooms/cabins?</strong>
                        </button>
                      </h5>
                    </div>
                    <div id="collapse23" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                      <div class="card-body">
                          No.
                      </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include(root.'templates/footer.php');
?>
