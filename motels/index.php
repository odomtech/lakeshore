<br>
<!-- motels -->
<div class="card w-100 mx-auto shadow p-3 mb-5 bg-white rounded">
    <div class="card-body">
        <div class="card-title text-center">
            <h2 class="mx-auto">Motel Units at Possum Kingdom Lake</h2>
            <p>Summer Rate: $99.00/Night<br>Holiday Rate: $126.50/Night<br>Fall Rate: $66.00/Night</p>
        </div>
        <div class="row">
            <div class="col-sm-7">
                <img src="<?=root?>assets/images/motel.jpg" width="100%" alt="">
            </div>
            <div class="col-sm-4 text-center">
                <img src="<?=root?>assets/images/motel/map.png" width="100%" alt="" id="map" data-toggle="modal" data-target="#exampleModal">
                <sub>click for larger map</sub>
            </div>
        </div>
        <hr>
        <div class="card-deck">
          <div class="card">
              <div class="card-header">
                  <h5 class="">Motel 5</h5>
              </div>
            <img class="card-img-top" src="<?=root?>assets/images/motel/5.jpg" alt="Card image cap">
            <div class="card-body text-center">
              <a href="<?=root?>reservations/?id=motel5" class="btn btn-primary btn-lg btn-block">Reservation</a>
            </div>
          </div>
          <div class="card">
              <div class="card-header">
                  <h5 class="">Motel 6</h5>
              </div>
            <img class="card-img-top" src="<?=root?>assets/images/motel/6.jpg" alt="Card image cap">
            <div class="card-body text-center">
              <a href="<?=root?>reservations/?id=motel6" class="btn btn-primary btn-lg btn-block">Reservation</a>
            </div>
          </div>
          <div class="card">
              <div class="card-header">
                  <h5 class="">Motel 7</h5>
              </div>
            <img class="card-img-top" src="<?=root?>assets/images/motel/7.jpg" alt="Card image cap">
            <div class="card-body text-center">
              <a href="<?=root?>reservations/?id=motel7" class="btn btn-primary btn-lg btn-block">Reservation</a>
            </div>
          </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
      <div class="modal-body">
        <img src="<?=root?>assets/images/map.jpg"  alt="" id="map">
      </div>

    </div>
  </div>
</div>
