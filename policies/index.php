<?php
define('root','../');
$URL = "Policies";
include(root.'includes/config.php');
include(root.'templates/header.php');
// Logo - Social/Connect Bar
include(root.'templates/socialbar.php');
// Navbar
include(root.'templates/navbar.php');
?>
<br>
<div class="container">
    <div class="card w-100 mx-auto shadow p-3 mb-5 bg-white rounded">
        <div class="card-body">
            <div class="card-title">
                <h2 class="text-center">Policies for Lakeshore Lodge & RV on Possum Kingdom Lake</h2>
                <p class="text-center">We thank you for choosing Lakeshore Lodge as your vacation destination. Please help us in keeping our grounds beautiful, peaceful, and litter fre by following these very important rules and regulations.</p>
            </div>
            <div class="card-text policy">
                <div class="row">
                    <div class="col">
                        <h3 class="text-center">CANCELLATION POLICY</h3>
                        <i>
                            <ul class="policy fa-ul">
                                <li><span class="fa-li"><i class="far fa-dot-circle"></i></span><span>There is a </span><strong><u>$50 cancelation fee</u></strong><span> for all reservations canceled 3 weeks or more prior to the reservation date.</span></li><br>
                                <li><span class="fa-li"><i class="far fa-dot-circle"></i></span><span>There will be <strong><u>no refunds</u></strong> for all reservations canceled less than 3 weeks before the reservation date.</span></li><br>
                                <li><span class="fa-li"><i class="far fa-dot-circle"></i></span><span><strong><u>No Refunds allowed for holiday weekends.</u></strong> </span></li><br>
                                <li><span class="fa-li"><i class="far fa-dot-circle"></i></span><span>Taxes are non-refundable.</span></li><br>
                                <li><span class="fa-li"><i class="far fa-dot-circle"></i></span><span>Cancellation Policies will be strictly enforced.</span></li><br>
                            </ul>
                        </i>
                        <hr>
                        <h3 class="text-center">GOLF CART RULES</h3>
                        <i>
                            <ul class="fa-ul">
                                <li><span class="fa-li"><i class="far fa-dot-circle"></i></span>There will be a charge of $15.00 per month for golf cart use. This charge is to cover electricity for charging the batteries, etc.</li><br>
                                <li><span class="fa-li"><i class="far fa-dot-circle"></i></span>Please use golf carts only to go from place to place and not for entertainment.</li><br>
                                <li><span class="fa-li"><i class="far fa-dot-circle"></i></span>Please do not park and leave your golf carts at the dock.</li><br>
                                <li><span class="fa-li"><i class="far fa-dot-circle"></i></span>Children under the age of 16 are not allowed to drive golf carts without adult supervision. The liability and accident risks are too high to allow children to spend their time circling the park on golf carts.</li><br>
                                <li><span class="fa-li"><i class="far fa-dot-circle"></i></span>No ATV'S or UTV'S</li><br>
                            </ul>
                        </i>
                        <hr>
                        <h3 class="text-center">MISCELLANEOUS RULES AND REGULATIONS</h3>
                        <i>
                            <ul class="fa-ul">
                                <li><span class="fa-li"><i class="far fa-dot-circle"></i></span>Lakeshore Lodge is a smoke free premise. There will be a $100.00 charge to your credit card for smoking in cabins, motels, and trailers. You may smoke outside but please pick up your cigarette butts. There will be a $30.00 charge to your credit card for not picking up trash or cigarette butts.</li><br>
                                <li><span class="fa-li"><i class="far fa-dot-circle"></i></span>All RV's must have at least a 6 foot clearance on the backside.</li><br>
                                <li><span class="fa-li"><i class="far fa-dot-circle"></i></span>All boat slips are reserved through our office. Please do not use the boat dock without reservations.</li><br>
                                <li><span class="fa-li"><i class="far fa-dot-circle"></i></span>The outside bathrooms are for reunion and meeting hall guests only.</li><br>
                                <li><span class="fa-li"><i class="far fa-dot-circle"></i></span>We have a noise curfew starting at 12am daily so for the benefit of other guests, please make sure the noise is at a minimum.</li><br>
                                <li><span class="fa-li"><i class="far fa-dot-circle"></i></span>We do not allow tents, campfires, fire pits, or fireworks.</li><br>
                                <li><span class="fa-li"><i class="far fa-dot-circle"></i></span>There is a $50.00 fee for all boats and trailers left on the property after departure.</li><br>
                                <li><span class="fa-li"><i class="far fa-dot-circle"></i></span>NO ATV'S or UTV's<br></li><br>
                            </ul>
                        </i>

                    </div>
                    <div class="col">
                        <h3 class="text-center">RESERVATION POLICTY</h3>
                        <i>
                        <ul class="fa-ul">
                            <li><span class="fa-li"><i class="far fa-dot-circle"></i></span><span>Check-in time is at 2:00 pm and check-out time is at 12:00 pm (This also includes RV's). There is a $20.00 charge per hour for early check-ins and late check-outs. Monthly and yearly RV's will be charged an extra day for electricity.</span></li><br>
                            <li><span class="fa-li"><i class="far fa-dot-circle"></i></span><span>All boat slips are reserved through our office. Please do not use the boat dock without reservations!</span></li><br>
                            <li><span class="fa-li"><i class="far fa-dot-circle"></i></span><span>Please check in with the office in regard to how many vehicles are permitted for your cabin or RV space. All other vehicles are to be parked in the designated boat parking area. This will help with the safety and room issues that arise when boats and vehicles are parked along the road and near cabins and RV's. There will be a $50.00 fee for parking boats or trailers in non-designated parking areas. Fees will apply for extra vehicles parked at the cabins or RV spaces.</span></li><br>
                            <li><span class="fa-li"><i class="far fa-dot-circle"></i></span><span>Your Name, Address, Phone Number is Required.</span></li><br>
                            <li><span class="fa-li"><i class="far fa-dot-circle"></i></span><span>Include all children over the age of 3 when making reservations.</span></li><br>
                            <li><span class="fa-li"><i class="far fa-dot-circle"></i></span><span>Cabins are reserved a minimum of two nights on regular weekends and three nights on holiday weekends.</span></li><br>
                            <li><span class="fa-li"><i class="far fa-dot-circle"></i></span><span>Cancellations must be made at least 3 weeks prior to reservation date. See cancelation policy for details.</span></li><br>
                            <li><span class="fa-li"><i class="far fa-dot-circle"></i></span><span>A cash or check deposit for one night's rent must be received within five days after making reservation or reservation will be canceled without further notice.</span></li><br>
                            <li><span class="fa-li"><i class="far fa-dot-circle"></i></span><span>Rates are subject to change without notice.</span></li><br>
                            <li><span class="fa-li"><i class="far fa-dot-circle"></i></span><span>Weekday and weekly rates are available.<br></span></li><br>
                        </ul>
                    </i>
                    <hr>
                    <h3 class="text-center">PET RULES AND REGULATIONS</h3>
                    <i>
                        <ul class="fa-ul">
                            <li><span class="fa-li"><i class="far fa-dot-circle"></i></span>There is a $10.00 charge daily for pets occupying cabins or trailers.</li><br>
                            <li><span class="fa-li"><i class="far fa-dot-circle"></i></span>All pets must be on a leash at all times.</li><br>
                            <li><span class="fa-li"><i class="far fa-dot-circle"></i></span>There will be a $25.00 fee for pets without a leash.</li><br>
                            <li><span class="fa-li"><i class="far fa-dot-circle"></i></span>Owners are responsible for pet damages and clean up.<br><span></span><br></li><br>
                        </ul>
                    </i>
                    </div>
                    <div class="col-12">
                        <hr>
                        <h3 class="text-center">DEPARTURE CHECKLIST</h3>
                        <ol class="ml-5 ">
                            <li>Please clean up cigarette butts and trash from the grounds around your area.</li>
                            <li>Please move all furniture (inside and outside) to its original place.</li>
                            <li>Please carry all trash to the dumpster.</li>
                            <li>Please clean out all food and beverages from the refrigerator.</li>
                            <li>Please clean all dirty dishes.</li>
                            <li>Please leave all linens and pillows on the bed.</li>
                            <li>RV's please keep any hoses, cables, and other wires under your RV.</li>
                            <li>Please make sure all air conditioners and other electrical items are turned off or you will be charged a daily electric fee.</li>
                            <li>All canopies, or furniture of any kind, which makes it difficult to mow and weed eat around, must be taken down or stored.</li>
                            <li>Please remember to leave your key at the office. There will be a $5.00 charge to your credit card for all lost or non-returned keys.</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include(root.'templates/footer.php');
?>
