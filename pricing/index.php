<?php
define('root','../');
if(!isset($_GET['action'])){
    $URL = "Pricing";
}

include(root.'includes/config.php');
include(root.'templates/header.php');
// Logo - Social/Connect Bar
include(root.'templates/socialbar.php');
// Navbar
include(root.'templates/navbar.php');
?>
<div class="container">
<?php
    if(isset($_GET['action']) && $_GET['action'] == 'cabins'){
        include(root."cabins/index.php");
    ?>

    <?php
    }else if(isset($_GET['action']) && $_GET['action'] == 'rv-parks'){
        include(root."rv-parks/index.php");
    ?>

    <?php
    }else if(isset($_GET['action']) && $_GET['action'] == 'motels'){
        include(root.'motels/index.php');
    ?>

    <?php
    }else{

    ?>
    <br>
    <div class="card">
        <div class="sectionheader mx-auto mt-1">
            <h2 class="mx-auto"><?=date("Y")?> Pricing for Lakeshore Lodge and RV at Possum Kingdom</h2>
        </div>
        <hr>
        <div class="section ml-4">
            <h4>Cabins & Mobile Home Rentals</h4>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="card w-auto mx-2 mb-1">
                <h5 class="card-header">Cabin 1</h5>
                  <div class="card-body">
                    <!-- <h5 class="card-title"></h5> -->
                    <p class="card-text">Two Rooms w/ one queen size bed & two full size beds, one sofa bed, kitchen, one bath, table & chairs, two vehicle passes. (Maximum number allowed: 8)</p>
                    <ul class="list-group ">
                        <li class="list-group-item">Summer: $165/night</li>
                        <li class="list-group-item">Holiday: $195.50/night</li>
                        <li class="list-group-item">Fall: $121/night</li>
                    </ul>
                    <br>
                    <a href="<?=root?>reservations/?id=cabin1" class="btn btn-primary">Book Cabin</a>
                  </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="card w-auto mx-2 mb-1">
                <h5 class="card-header">Cabin 2</h5>
                  <div class="card-body">
                    <!-- <h5 class="card-title"></h5> -->
                    <p class="card-text">Two Rooms w/ one queen size bed & two full size beds, one sofa bed, kitchen, one bath, table & chairs, two vehicle passes. (Maximum number allowed: 8)</p>
                    <ul class="list-group ">
                        <li class="list-group-item">Summer: $165/night</li>
                        <li class="list-group-item">Holiday: $195.50/night</li>
                        <li class="list-group-item">Fall: $121/night</li>
                    </ul>
                    <br>
                    <a href="<?=root?>reservations/?id=cabin2" class="btn btn-primary">Book Cabin</a>
                  </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="card w-auto mx-2 mb-1">
                <h5 class="card-header">Cabin 3</h5>
                  <div class="card-body">
                    <!-- <h5 class="card-title"></h5> -->
                    <p class="card-text">Two rooms w/ one queen size bed & three full size beds, kitchen, one bath, table & chairs. Two vehicle passes. (Maximum number allowed: 8)</p>
                    <ul class="list-group ">
                        <li class="list-group-item">Summer: $165/night</li>
                        <li class="list-group-item">Holiday: $195.50/night</li>
                        <li class="list-group-item">Fall: $121/night</li>
                    </ul>
                    <br>
                    <a href="<?=root?>reservations/?id=cabin3" class="btn btn-primary">Book Cabin</a>
                  </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="card w-auto mx-2 mb-1">
                <h5 class="card-header">Cabin 4</h5>
                  <div class="card-body">
                    <!-- <h5 class="card-title"></h5> -->
                    <p class="card-text">Two Rooms w/ one queen size bed & two full size beds, one sofa bed, kitchen, one bath, table & chairs, two vehicle passes. (Maximum number allowed: 8)</p>
                    <ul class="list-group ">
                        <li class="list-group-item">Summer: $165/night</li>
                        <li class="list-group-item">Holiday: $195.50/night</li>
                        <li class="list-group-item">Fall: $121/night</li>
                    </ul>
                    <br>
                    <a href="<?=root?>reservations/?id=cabin2" class="btn btn-primary">Book Cabin</a>
                  </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="card w-auto mx-2 mb-1">
                <h5 class="card-header">Cabin 3 & 4 Combined</h5>
                  <div class="card-body">
                    <!-- <h5 class="card-title"></h5> -->
                    <p>Combined rentals of cabins 3 & 4.<br><br><br></p>
                    <ul class="list-group ">
                        <li class="list-group-item">Summer: $319/night</li>
                        <li class="list-group-item">Holiday: $385.25/night</li>
                        <li class="list-group-item">Fall: $220/night</li>
                    </ul>
                    <br>
                    <a href="<?=root?>reservations/?id=cabin34" class="btn btn-primary">Book Cabin</a>
                  </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="card w-auto mx-2 mb-1">
                <h5 class="card-header">Cabin 5</h5>
                  <div class="card-body">
                    <!-- <h5 class="card-title"></h5> -->
                    <p class="card-text">Three rooms w/ three queen size beds, one full size bed, three bunk beds, two futons in living room, two full size baths, full kitchen, table & chairs. Three vehicle passes. (Maximum Number Allowed: 20)</p>
                    <ul class="list-group ">
                        <li class="list-group-item">Summer: $319/night</li>
                        <li class="list-group-item">Holiday: $385.25/night</li>
                        <li class="list-group-item">Fall: $220/night</li>
                    </ul>
                    <br>
                    <a href="<?=root?>reservations/?id=cabin5" class="btn btn-primary">Book Cabin</a>
                  </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="card w-auto mx-2 mb-1">
                <h5 class="card-header">Mobile Home #2</h5>
                  <div class="card-body">
                    <!-- <h5 class="card-title"></h5> -->
                    <p>One bedroom w/ one queen size bed, living area w/ a sofa bed, kitchen, one bath, table & chairs, one vehicle pass. (Maximum Number Allowed: 4)</p>
                    <ul class="list-group ">
                        <li class="list-group-item">Summer: $115.50/night</li>
                        <li class="list-group-item">Holiday: $143.75/night</li>
                        <li class="list-group-item">Fall: $88/night</li>
                    </ul>
                    <br>
                    <a href="<?=root?>reservations/?id=mobile2" class="btn btn-primary">Book Cabin</a>
                  </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="card w-auto mx-2 mb-1">
                <h5 class="card-header">Mobile Home 3, 4, 5, 6, & 7</h5>
                  <div class="card-body">
                    <!-- <h5 class="card-title"></h5> -->
                    <p class="card-text">Three bedroom, one bath, fully equipped kitchen, three full size beds, one sofa bed. #3 has two baths & is located on a secluded cove. Two vehicle passes per home. (Maximum Number Allowed: 8)</p>
                    <ul class="list-group ">
                        <li class="list-group-item">Summer: $165/night</li>
                        <li class="list-group-item">Holiday: $195.50/night</li>
                        <li class="list-group-item">Fall: $121/night</li>
                    </ul>
                    <br>
                    <a href="<?=root?>reservations/?id=mobile" class="btn btn-primary">Book Cabin</a>
                  </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="card w-auto mx-2 mb-1">
                <h5 class="card-header">Double Wide</h5>
                  <div class="card-body">
                    <!-- <h5 class="card-title"></h5> -->
                    <p>Six twin beds, two full beds, two queen beds, one fold out futon, & one hideaway bed. Waterfront deck overlooking the lake w/ charcoal grill. (Sleeps 18)</p>
                    <ul class="list-group ">
                        <li class="list-group-item">Summer: $319/night</li>
                        <li class="list-group-item">Holiday: $385.25/night</li>
                        <li class="list-group-item">Fall: $220/night</li>
                    </ul>
                    <br>
                    <a href="<?=root?>reservations/?id=doublewide" class="btn btn-primary">Book Cabin</a>
                  </div>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="card w-auto mx-2 mb-1">
                <h5 class="card-header">A-Frame House</h5>
                  <div class="card-body">
                    <!-- <h5 class="card-title"></h5> -->
                    <p class="card-text">Two stories-upstairs there is one full size bed, two twin beds, & one bath. Downstairs-has one bedroom w/ two full size beds, one bath, full kitchen w/ bar that separates living area which has one sofa bed, couch, table & chairs. Also has a private back patio overlooking the lake. Three vehicle passes. (Maximum Number Allowed: 10)</p>
                    <ul class="list-group ">
                        <li class="list-group-item">Summer: $264/night</li>
                        <li class="list-group-item">Holiday: $316.25/night</li>
                        <li class="list-group-item">Fall: $198/night</li>
                    </ul>
                    <br>
                    <a href="<?=root?>reservations/?id=aframe" class="btn btn-primary">Book Cabin</a>
                  </div>
            </div>
          </div>
        </div>

        <hr>
        <div class="section ml-4">
            <h4>The Recreational Lodge</h4>
            <div class="card w-auto mx-2 mb-1">
                <h5 class="card-header">The Lodge</h5>
                  <div class="card-body">
                    <!-- <h5 class="card-title"></h5> -->
                    <p class="card-text">Table, chairs, full kitchen, & a screened in porch overlooking the lake. Ideal location for family reunions, class reunions, & meetings</p>
                    <ul class="list-group ">
                        <li class="list-group-item">Summer: $264/night</li>
                        <li class="list-group-item">Holiday: $316.25/night</li>
                        <li class="list-group-item">Fall: $198/night</li>
                    </ul>
                    <br>
                    <a href="<?=root?>reservations/?id=lodge" class="btn btn-primary">Reserve The Lodge</a>
                  </div>
            </div>
        </div>
        <hr>
        <div class="section ml-4">
            <h4>Motel</h4>
            <div class="card w-auto mx-2 mb-1">
                <h5 class="card-header">Units 5, 6, & 7</h5>
                  <div class="card-body">
                    <!-- <h5 class="card-title"></h5> -->
                    <!-- <p class="card-text">Table, chairs, full kitchen, & a screened in porch overlooking the lake. Ideal location for family reunions, class reunions, & meetings</p> -->
                    <ul class="list-group ">
                        <li class="list-group-item">Summer: $99/night</li>
                        <li class="list-group-item">Holiday: $126.50/night</li>
                        <li class="list-group-item">Fall: $66/night</li>
                    </ul>
                    <br>
                    <a href="<?=root?>reservations/?id=motel" class="btn btn-primary">Book Room</a>
                  </div>
            </div>
        </div>
        <hr>
        <div class="section ml-4">
            <h4>RV Park</h4>
            <div class="row">
              <div class="col-sm-6">
                <div class="card w-auto mx-2 mb-1">
                    <h5 class="card-header">30 Amp</h5>
                      <div class="card-body">
                        <!-- <h5 class="card-title"></h5> -->
                        <p></p>
                        <ul class="list-group ">
                            <li class="list-group-item">Summer: $45/night</li>
                            <li class="list-group-item">Holiday: $55/night</li>
                            <li class="list-group-item">Fall: $45/night</li>
                        </ul>
                        <br>
                        <a href="<?=root?>reservations/?id=30amp" class="btn btn-primary">Reserve Spot</a>
                      </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="card w-auto mx-2 mb-1">
                    <h5 class="card-header">50 Amp</h5>
                      <div class="card-body">
                        <!-- <h5 class="card-title"></h5> -->
                        <p></p>
                        <ul class="list-group ">
                            <li class="list-group-item">Summer: $57.50/night</li>
                            <li class="list-group-item">Holiday: $75/night</li>
                            <li class="list-group-item">Fall: $57.50/night</li>
                        </ul>
                        <br>
                        <a href="<?=root?>reservations/?id=50amp" class="btn btn-primary">Reserve Spot</a>
                      </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6">
                <div class="card w-auto mx-2 mb-1">
                    <h5 class="card-header">Monthly Rate</h5>
                      <div class="card-body">
                        <!-- <h5 class="card-title"></h5> -->
                        <p>$275/month <sup>1</sup></p>
                        <sub>See Details</sub>
                        <br>
                        <!-- <a href="<?=root?>reservations/?id=30amp" class="btn btn-primary">Book Cabin</a> -->
                      </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="card w-auto mx-2 mb-1">
                    <h5 class="card-header">Annual Rate</h5>
                      <div class="card-body">
                        <!-- <h5 class="card-title"></h5> -->
                        <p>$200/month <sup>1</sup></p>
                        <sub>See Details</sub>

                        <br>
                        <!-- <a href="<?=root?>reservations/?id=50amp" class="btn btn-primary">Book Cabin</a> -->
                      </div>
                </div>
              </div>
            </div>

            <div class="card w-auto mx-2 mb-1">
                <h5 class="card-header">Boat Slips</h5>
                  <div class="card-body">
                    <!-- <h5 class="card-title"></h5> -->
                    <p class="card-text">$15/day <sup>2</sup> </p>
                    <sub>See Details</sub>
                    <br>

                  </div>
            </div>
            <div class="card w-auto mx-2 mb-1">
                <h5 class="card-header">Pets</h5>
                  <div class="card-body">
                    <h5 class="card-title">Pet Fee <br>$10 per day</h5>
                    <p class="card-text">ALL PETS MUST BE ON  A LEASH AT ALL TIMES! Owner of pet is responsible for all damages & clean-up. </p>
                    <br>

                  </div>
            </div>
            <div class="col-12 text-center">

                <br>
                <h6><sup>1</sup> Daily Electricity Usage Fee for monthly & yearly rates</h6>
                <p>30 AMP: $10/day<br>50 AMP: $12.50/day</p>
                <p> <sup>2</sup> Boat slips are only available for resort guests only and based on availability.</p>
                <p><sup>3</sup> Parking of Boats and/or trailers is not allowed around RV spots. Only 1 vehicle allowed parked with RV or Camper.</p>
                <p>* Reservations are made for two nights on regular weekends and three nights on holiday weekends.
                    One night deposit is required for reservations. Cancellations must be made 3 weeks in advance to receive a refund less a $50.00 Cancellation fee.
                    Additional people over maximum number allowed will be charged $10.00 per person per day. Please see <a href="<?=root?>policies">Policies Page</a> for further fees and regulations.</p>

            </div>
        </div>
        </div>
    </div>
<br>


    <?php
    }

?>
</div>

<!-- footer -->
<?php
include(root.'templates/footer.php');
?>
