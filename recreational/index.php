<?php
define('root','../');
$URL = "Boat Ramp";
include(root.'includes/config.php');
include(root.'templates/header.php');
// Logo - Social/Connect Bar
include(root.'templates/socialbar.php');
// Navbar
include(root.'templates/navbar.php');
?>
<br>
<div class="container">

    <div class="card w-100 mx-auto shadow p-3 mb-5 bg-white rounded">
        <div class="card-body">
            <div class="card-title text-center">
                <h2 class="mx-auto">Boat Dock at Lakeshore Lodge</h2>
            </div>
            <div class="card-text mx-auto">
                <div class="fotorama mx-auto text-center" data-nav="thumbs" data-allowfullscreen="true" data-width="100%" data-ratio="16/9" data-minwidth="400"  data-minheight="300" data-maxheight="100%">
                   <?php imageslideshow('boat'); ?>
                  </div>
                  <br>
                  <div class="price text-center">
                      <h4>Boat Slips</h4>
                      <p>$15.00 per day*</p>
                      <p>Boat slips are only available for resort guests only and based on availability.</p>
                  </div>
            </div>
        </div>
    </div>

</div>
<!-- footer -->
<?php
include(root.'templates/footer.php');
?>
