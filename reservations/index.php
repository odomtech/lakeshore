<?php
define('root','../');
$URL = "Reservations";
include(root.'includes/config.php');
include(root.'templates/header.php');
// Logo - Social/Connect Bar
include(root.'templates/socialbar.php');
// Navbar
include(root.'templates/navbar.php');
if(isset($_GET['id'])){
    $id = $_GET['id'];
}else{
    $id = null;
}
?>
<br>
<div class="container">
    <div class="card w-100 mx-auto shadow p-3 mb-5 bg-white rounded">
        <div class="card-body">
            <div class="card-title text-center">
                <h2 class="mx-auto">Reservations for Lakeshore Lodge and RV at Possum Kingdom Lake</h2>
                <p>* Cancellation on all cabins, motel rooms, and RV spaces made 3 weeks prior to reservation date are refunded, less a $50.00 cancellation fee. No refund or deposit on or after the 3 weeks prior to your reservation. No refunds will be issued for deposits paid for reservations made for Holiday Weekends. (Memorial Day, 4<sup>th</sup> of July weekends, and Labor day)<br><span class="text-danger font-weight-bold">check-in after 2:00PM - Check-out before 12:00PM (Cabins and RV's Included)</span></p>
            </div>
            <div class="card-text">
                <h3 class="mx-auto text-center">Request A Reservation</h3>
                <hr>
                <form class="" action="reservation.php" method="post" enctype="multipart/form-data">

                    <div class="form-row w-75 mx-auto">
                        <div class="form-group col-md-6">
                            <label for="name"><h5>First Name</h5></label>
                            <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name" >
                        </div>
                        <div class="form-group col-md-6">
                            <label for="name"><h5>Last Name</h5></label>
                            <input type="text" class="form-control" id="lname" name="lname" placeholder="last Name">
                        </div>

                    </div>
                    <div class="form-row w-75 mx-auto">
                        <div class="form-group col-md-6">
                            <label for="phone"><h5>Phone #:</h5></label>
                            <input type="text" class="form-control" id="phone" name="phone" placeholder="1234567890" >
                        </div>
                        <div class="form-group col-md-6">
                            <label for="email"><h5>Email</h5></label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email" >
                        </div>

                    </div>
                    <hr>
                    <h3 class="text-center">Lodging Selections</h3>
                    <p class="text-center">Reservations are made on a first come first serve basis. Please select your preferred lodging choices below. In the event neither of your choices are available you will be contacted to discuss other possibilities. Thank you!</p>
                    <div class="form-row ">
                        <div class="form-group col-sm-3">
                            <label for="date"><h5>Check-In Date</h5> </label>
                            <input type="date" class="form-control" id="datepicker" name="date" placeholder="Select A Date">
                        </div>
                        <div class="form-group col-sm-3">
                            <label for="nights"><h5>How many nights?</h5></label>
                            <input type="number" class="form-control" id="nights" name="nights">
                        </div>
                        <div class="form-group col-sm-3">
                            <label for="adults"><h5># of Adults(18yrs & Older)</h5></label>
                            <input type="number" class="form-control" id="adults" name="adults">
                        </div>
                        <div class="form-group col-sm-3">
                            <label for="children"><h5># of Children(17Yrs & Younger)</h5> </label>
                            <input type="number" class="form-control" id="children" name="children">
                        </div>
                    </div>
                    <div class="form-row w-75 mx-auto">
                        <div class="form-group col-md-6 ">
                            <label for="boatslip"><h5>Will you need a boat slip?</h5></label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="boatslip" id="boatYes" value="Yes" >
                                <label class="form-check-label" for="boatYes">
                                Yes
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="boatslip" id="boatNo" value="No">
                                <label class="form-check-label" for="boatNo">
                                No
                                </label>
                            </div>
                            <div class="form-check disabled">
                                <input class="form-check-input" type="radio" name="boatslip" id="boatNotSure" value="Unsure">
                                <label class="form-check-label" for="boatNotSure">
                                Not Sure
                                </label>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="pets"><h5>Will you be bringing any pets?</h5></label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="pets" id="petsYes" value="Yes" >
                                <label class="form-check-label" for="petsYes">
                                Yes
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="pets" id="petsNo" value="No" >
                                <label class="form-check-label" for="petsNo">
                                No
                                </label>
                            </div>
                            <div class="form-check disabled">
                                <input class="form-check-input" type="radio" name="pets" id="petsNotSure" value="unsure">
                                <label class="form-check-label" for="petsNotSure">
                                Not Sure
                                </label>
                            </div>
                        </div>

                    </div>
                    <div class="form-group w-50 tex-center mx-auto">
                        <label for="lodging"> <h5>Cabin/Mobile Home/Motel/RV Slot</h5></label>
                        <select class="form-control" id="lodging" name="lodging">
                            <option>-- Please Choose Lodging Preference --</option>
                            <option value="cabin">Cabin</option>
                            <option value="mobile">Mobile Home</option>
                            <option value="motel">Motel Room</option>
                            <option value="rv-slot">RV Slot</option>

                        </select>
                    </div>
                    <div class="form-row w-50 mx-auto" >
                        <div class="form-group col">
                            <label for="choice1"><h5>1<sup>st</sup> Choice</h5></label>
                            <select class="form-control" id='choice1' name="choice1" disabled>
                                <option value="null">-- Please Choose --</option>
                            </select>
                        </div>
                        <div class="form-group col">
                            <label for="choice2"><h5>2<sup>nd</sup> Choice</h5></label>
                            <select class="form-control" id='choice2' name="choice2" disabled>
                                <option value="null">-- Please Choose --</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group minimap text-center mx-auto">
                        <img src="<?=root?>assets/images/motel/map.png" width="5%" alt="" id="map" data-toggle="modal" data-target="#exampleModal"><br>
                        <sub>click for view map</sub>
                    </div>
                    <div class="form-group mx-auto text-center">
                        <button type="submit" id='reservationRequest' class="btn btn-lg btn-primary">Send Request</button>
                    </div>

                </form>
            </div>
        </div>
    </div>

    <br>

</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
      <div class="modal-body">
        <img src="<?=root?>assets/images/map.jpg"  alt="" id="map">
      </div>

    </div>
  </div>
</div>
<!-- footer -->
<?php
include(root.'templates/footer.php');
?>
