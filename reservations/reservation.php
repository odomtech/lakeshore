<?php
define('root','../');
include(root."includes/config.php");
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
if($_SERVER['REQUEST_METHOD'] == "POST"){
    // var_dump($_POST);
    $guestTotal = $_POST['adults']+$_POST['children'];

    $guestDetails = "<ul>
        <li>First Name: {$_POST['fname']}</li>
        <li>Last Name: {$_POST['lname']}</li>
        <li>Phone #: {$_POST['phone']}</li>
        <li>Email: {$_POST['email']}</li>
        <hr>
        <li>Check-in Date: {$_POST['date']}</li>
        <li># of nights: {$_POST['nights']}</li>
        <li>Number of guests: {$guestTotal} Total with {$_POST['adults']}-Adults and {$_POST['children']}-Children</li>
        <li>Boat Slip Needed: {$_POST['boatslip']}</li>
        <li>Pets: {$_POST['pets']}</li>
        <li>Lodging Preference: {$_POST['lodging']}</li>
        <li>1st Choice: {$_POST['choice1']}</li>
        <li>2nd Choice: {$_POST['choice2']}</li>
    </ul>";
    $body = "
    <h3>A New Reservation Requst From </h3>
    <p>Here are the details of the reservation:</p><br>
    {$guestDetails}
    <p></p>
    ";
    // echo $body;
    guestMailer($guestDetails);
    if(resortMailer($body)){
        // echo "succeeded!";
        redirect_to(root.'reservations/thankyou.php?name='.$_POST['fname']);
    }else{
        echo "failed!";
    }
}
function resortMailer($body){
    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
    try {
        //Server settings
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'mail.odomtechservices.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'jonathan@odomtechservices.com';                 // SMTP username
        $mail->Password = 'Odie0309';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom($_POST['email'], $_POST['fname'].' '.$_POST['lname']);
        $mail->addAddress('mail@lakeshoreonpk.com', 'Reservation Request');     // Add a recipient
        // $mail->addAddress('ellen@example.com');               // Name is optional
        $mail->addReplyTo($_POST['email'], $_POST['fname'].' '.$_POST['lname']);
        // $mail->addCC('cc@example.com');
        // $mail->addBCC('bcc@example.com');

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'A New Reservation Request From '.$_POST['fname'].' '.$_POST['lname'];
        $mail->Body = $body;
        // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        $mail->send();
        return true;
    } catch (Exception $e) {
        echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
    }
}

function guestMailer($guestDetails){
    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
    try {
        //Server settings
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'mail.odomtechservices.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'jonathan@odomtechservices.com';                 // SMTP username
        $mail->Password = 'Odie0309';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom('mail@lakeshoreonpk.com', 'Reservation Request');
        $mail->addAddress($_POST['email'], $_POST['fname'].' '.$_POST['lname']);     // Add a recipient
        // $mail->addAddress('ellen@example.com');               // Name is optional
        $mail->addReplyTo('mail@lakeshoreonpk.com', 'Lakeshore Lodge');
        // $mail->addCC('cc@example.com');
        // $mail->addBCC('bcc@example.com');

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Your Reservation Request From Lakeshore Lodge on Possum Kingdom Lake';
        $mail->Body    = 'Hi, '.$_POST['fname'].'<br><br>Thank your for your interest in staying here at Lakeshore Lodge on PK. Your request for reservation has been submitted, someone will give you a call shortly to discuss your stay in more detail and confirm your reservation and set up your deposit. Here are the detail you requested for your reservation:'.$guestDetails.'<br>Please visit our <a href="https://lakeshoreonpk.com/policies">Cancellation and Reservation Policies</a> page for information regarding your stay. If you have any questions before you speak with us about your stay please visit the <a href="https://lakeshoreonpk.com/faq">Frequently Asked Questions</a> page <br><br>Thank You,<br>Lakeshore Lodge & RV Park<br><a href="https://lakeshoreonpk.com">www.lakeshoreonpk.com</a><br><a href="tel:+19405492518">(940)549-2518</a><br><br><br> This message was automatically generated by our reservation system. Your information is only transmitted in this email and never saved in any way except within this message.';
        // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        $mail->send();
        // echo 'Message has been sent';

    } catch (Exception $e) {
        echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
    }
}
?>
