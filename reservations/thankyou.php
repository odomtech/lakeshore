<?php
define('root','../');
$URL = "Reservations";
include(root.'includes/config.php');
include(root.'templates/header.php');
// Logo - Social/Connect Bar
include(root.'templates/socialbar.php');
// Navbar
include(root.'templates/navbar.php');
if(isset($_GET['name'])){
    // code...
    $guest = $_GET['name'];
}else{
    $guest = null;
    
}
?>
<div class="container">
    <div class="card w-100 mx-auto shadow p-3 mb-5 bg-white rounded">
        <div class="card-body">
            <div class="card-title text-center">
                <h2 class="mx-auto">Reservations for Lakeshore Lodge and RV at Possum Kingdom Lake</h2>
                <p>* Cancellation on all cabins, motel rooms, and RV spaces made 3 weeks prior to reservation date are refunded, less a $50.00 cancellation fee. No refund or deposit on or after the 3 weeks prior to your reservation. No refunds will be issued for deposits paid for reservations made for Holiday Weekends. (Memorial Day, 4<sup>th</sup> of July weekends, and Labor day)<br><span class="text-danger font-weight-bold">check-in after 2:00PM - Check-out before 12:00PM (Cabins and RV's Included)</span></p>
            </div>
            <div class="card-text text-center">

                <h3>Hi <?=$guest?>, thank you for your request for a reservation. Please check your email for a confirmation that we received your request. Someone from Lakeshor Lodge will be in touch with you as soon as possible to confirm your stay and set up the reservation and deposit.</h3>
                <h3>Please check our <a href="<?=root?>policies">Policies</a> page for important information regarding your stay here with us at Lakeshore Lodge On PK.</h3>
                <h3>If you have any questions please visit the <a href="<?=root?>faq">Frequently Asked Questions</a> page. If you don't find an answer here give us a call at (940)549-2518</h3>
            </div>
        </div>
    </div>
</div>
<?php
include(root.'templates/footer.php');
?>
