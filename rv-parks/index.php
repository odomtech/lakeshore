<br>
<!-- motels -->
<div class="card w-100 mx-auto shadow p-3 mb-5 bg-white rounded">
    <div class="card-body">
        <div class="card-title text-center">
            <h2 class="mx-auto">RV Park at Possum Kingdom Lake</h2>
            <p>Lakeshore Lodge has availbale RV Spots with 30amp and 50amp electric options. Monthly & yearly spots are available. When staying long term you only pay an electricity usage fee for the days you are on site.</p>

                <div class="text-center">
                  <a href="<?=root?>pricing" class="btn btn-lg  btn-primary mx-auto">Price List</a>
                </div>
        </div>
        <div class="row">
            <div class="col-sm-7">
                <div class="fotorama" data-nav="thumbs" data-allowfullscreen="true" data-width="100%" data-ratio="16/9" data-minwidth="400" data-maxwidth="1000" data-minheight="300" data-maxheight="100%">
                   <?php imageslideshow('rv'); ?>
                  </div>
            </div>
            <div class="col-sm-4 text-center">
                <img src="<?=root?>assets/images/motel/map.png" width="100%" alt="" id="map" data-toggle="modal" data-target="#exampleModal">
                <sub>click for larger map</sub>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm text-center">
                <h4>RV 30 AMP</h4>
                <p>Summer Rate: $45.00/Night<br>
                Holiday Rate: $55.00/Night<br>
                Fall Rate: $45.00/Night</p>


            </div>
            <div class="col-sm text-center">
                <h4>RV 50 AMP</h4>
                <p>Summer Rate: $57.50/Night<br>
                Holiday Rate: $75.00/Night<br>
                Fall Rate: $57.50/Night</p>


            </div>
            <div class="col-sm text-center">
                <h4>Monthly/Yearly</h4>
                <p>Monthly Rate: $275.00/Month <sup>1</sup> <br>
                    6 Months Rate: $250.00/Month <sup>1</sup><br>
                Yearly Rate: $200.00/Month <sup>1</sup>
            </div>
            <div class="col-12 text-center">
                <a href="<?=root?>reservations/?id=rv" class="btn btn-lg btn-primary btn-block">Reserve A Spot</a>
                <br>
                <h6><sup>1</sup> Daily Electricity Usage Fee for monthly & yearly rates</h6>
                <p>30 AMP: $10/day<br>50 AMP: $12.50/day</p>
                <p><sup>2</sup> Parking of Boats and/or trailers is not allowed around RV spots. Only 1 vehicle allowed parked with RV or Camper.</p>
                <p>* Reservations are made for two nights on regular weekends and three nights on holiday weekends.
                    One night deposit is required for reservations. Cancellations must be made 3 weeks in advance to receive a refund less a $50.00 Cancellation fee.
                    Additional people over maximum number allowed will be charged $10.00 per person per day. Please see <a href="<?=root?>policies">Policies Page</a> for further fees and regulations.</p>

            </div>
        </div>


    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
      <div class="modal-body">
        <img src="<?=root?>assets/images/map.jpg"  alt="" id="map">
      </div>

    </div>
  </div>
</div>
