<div class="container">
    <div class="d-flex justify-content-center">
        <p>*Be sure to review our new <a href="<?=root?>policies">Cancellation and Reservation Policies</a> for <?php echo date("Y"); ?></p>
    </div>
</div>
<br>

<footer>
    <br>
    <div class="container  bottom">
        <div class="row">
            <div class="col-md-4">
                <ul class="list-unstyled text-left">
                    <li><a href="<?=root?>pricing"><h4>Pricing</h4></a></li>
                    <li><a href="<?=root?>pricing/?action=cabins"><h4>Cabins</h4></a></li>
                    <li><a href="<?=root?>pricing/?action=motels"><h4>Motels</h4></a></li>
                    <li><a href="<?=root?>pricing/?action=rv-parks"><h4>RV Park</h4></a></li>
                    <li><a href="<?=root?>thelodge"><h4>The Lodge</h4></a></li>
                </ul>
            </div>


            <div class="col-md-4">
                <ul class="list-unstyled text-left">
                    <li><a href="<?=root?>faq"><h4>FAQ's</h4></a></li>
                    <li><a href="<?=root?>policies"><h4>Policies</h4></a></li>
                    <li><a href="<?=root?>recreational"><h4>Boat Dock</h4></a></li>
                    <li><a href="<?=root?>recreational"><h4>Fishing</h4></a></li>


                </ul>

            </div>
            <div class="col-md-4 text-right">
                <h3 class="" style="color:white">Contact Us</h3>
                  <a style="color:white" href="mailto:mail@lakeshoreonpk.com">mail@lakeshoreonpk.com</a><br>
                  <p style="color:white">
                      5501 Lakeshore Drive<br>
                      Graham, TX 76450<br>
                      USA

                  </p>
                  <div class="row">
                      <div class="col-6">
                            <a href="tel:+19405492518" class="btn btn-success" name="button"><i class="fas fa-mobile-alt "></i> (940)549-2518</a>
                      </div>
                      <div class="col-6">
                          <button onclick="location.href = 'https://goo.gl/maps/8odsL92h8ws';" class="btn btn-success" type="button" name="button"><i class="fas fa-map-signs "></i> Directions</button>
                      </div>
                  </div>


            </div>
        </div>
    </div>
    <hr>
    <div class="container"
        <div class="row">
            <div class="col-xs-12 ">
              <p class="" style="color:white">Copyright &copy; <?php echo date("Y"); ?> <a href="/">Lakeshore Lodge & RV</a> | Designed By: <a href="https://odomtechservices.com">Odom Tech Services</a> </p>
            </div>
        </div>
    </div>
</footer>
    <script type="text/javascript" src="<?=root?>assets/font-awesome/fontawesome-all.min.js"></script>
    <!-- <script src="<?=root?>assets/jquery/jquery.min.js"></script> -->
    <script src="<?=root?>assets/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <!-- Core JS file -->
    <script src="<?=root?>assets/photoswipe/photoswipe.min.js"></script>

    <!-- UI JS file -->
    <script src="<?=root?>assets/photoswipe/photoswipe-ui-default.min.js"></script>
    <script type="text/javascript" src="<?=root?>assets/js/main.js"></script>
    </body>
</html>
