<nav class="navbar navbar-expand-lg navbar-light bg-dark navigation">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse " id="navbarNav">
    <ul class="navbar-nav align-items-center">
      <li class="nav-item <?php if($location == null){echo 'active';}; ?>">
        <a class="nav-link" href="<?=root?>">Home <span class="sr-only">(current)</span></a>
      </li>
      <!-- <li class="nav-item <?php if($location == 'pricing'){echo 'active';}; ?>">
        <a class="nav-link" href="<?=root?>pricing">Pricing</a>
      </li> -->
      <li class="nav-item dropdown <?php if($location == 'pricing'){echo 'active';}; ?>">
        <a class="nav-link dropdown-toggle" href="<?=root?>pricing" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Pricing
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="<?=root?>pricing/?action=cabins">Cabins</a>
          <a class="dropdown-item" href="<?=root?>pricing/?action=rv-parks">RV Park</a>
          <a class="dropdown-item" href="<?=root?>pricing/?action=motels">Motel</a>
        </div>
      </li>


      <li class="nav-item <?php if($location == 'thelodge'){echo 'active';}; ?>">
        <a class="nav-link" href="<?=root?>thelodge">The Lodge</a>
      </li>
      <li class="nav-item <?php if($location == 'reservations'){echo 'active';}; ?>">
        <a class="nav-link" href="<?=root?>reservations">Reservations</a>
      </li>
      <li class="nav-item <?php if($location == 'recreational'){echo 'active';}; ?>">
        <a class="nav-link" href="<?=root?>recreational">Boat Dock</a>
      </li>
      <li class="nav-item <?php if($location == 'faq'){echo 'active';}; ?>">
        <a class="nav-link" href="<?=root?>faq">FAQ's</a>
      </li>
      <li class="nav-item <?php if($location == 'policies'){echo 'active';}; ?>">
        <a class="nav-link" href="<?=root?>policies">Policies</a>
      </li>
    </ul>
  </div>
</nav>
