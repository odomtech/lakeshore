<?php
define('root','../');
include(root.'includes/config.php');
include(root.'templates/header.php');
// Logo - Social/Connect Bar
include(root.'templates/socialbar.php');
// Navbar
include(root.'templates/navbar.php');
?>
<div class="container">
    <br>
    <div class="card w-100 mx-auto shadow p-3 mb-5 bg-white rounded">
        <div class="card-body">
            <div class="card-title text-center">
                <h2 class="mx-auto">The Lakeshore Lodge at Possum Kingdom Lake</h2>
                <p>Tables, chairs, full kitchen, & a screened in porch overlooking the lake. Ideal location for family reunions, class reunions, & meetings<br>Give a call to book the Lodge for your large gathering.</p>
                <p>Summer Rate: $264.00/Night<br>Holiday Rate: $316.25/Night<br>Fall Rate: $198.00/Night</p>
                <div class="fotorama" data-nav="thumbs" data-allowfullscreen="true">
                <?php imageslideshow('lodge'); ?>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- footer -->
<?php
include(root.'templates/footer.php');
?>
